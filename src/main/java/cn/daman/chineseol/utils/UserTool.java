package cn.daman.chineseol.utils;

import cn.daman.chineseol.entity.User;
import cn.daman.chineseol.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;

@Service
public class UserTool {
    @Autowired
    private UserRepo userRepo;
    public User getUser(HttpSession session){
        Integer uid = (Integer) session.getAttribute("uid");
        if (uid!=null){
            return userRepo.findById(uid).get();
        }
        return null;
    }
}
