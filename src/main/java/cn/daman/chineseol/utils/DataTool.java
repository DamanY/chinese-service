package cn.daman.chineseol.utils;

import cn.daman.chineseol.repo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DataTool {
    @Autowired
    public AtcRepo atcRepo;
    @Autowired
    public UserRepo userRepo;
    @Autowired
    public NotPassAtcRepo notPassAtcRepo;
    @Autowired
    public CmtRepo cmtRepo;
    @Autowired
    public ReplyRepo replyRepo;
    @Autowired
    public MsgRepo msgRepo;
    @Autowired
    public ConfRepo confRepo;
}
