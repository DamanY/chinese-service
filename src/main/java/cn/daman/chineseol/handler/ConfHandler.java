package cn.daman.chineseol.handler;

import cn.daman.chineseol.utils.DataTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/conf")
public class ConfHandler {
    @Autowired
    private DataTool dataTool;
    @GetMapping("/get/{key}")
    public String get(@PathVariable String key){
        return dataTool.confRepo.findByTheKey(key).getTheValue();
    }
}
