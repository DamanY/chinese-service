package cn.daman.chineseol.handler;

import cn.daman.chineseol.entity.Article;
import cn.daman.chineseol.entity.NotPassAtc;
import cn.daman.chineseol.entity.User;
import cn.daman.chineseol.utils.DataTool;
import cn.daman.chineseol.utils.UserTool;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@RequestMapping("/do")
@Api("ActionHandler")
public class ActionHandler {
    @Autowired
    private DataTool dataTool;
    @Autowired
    private UserTool userTool;
    @GetMapping("/like")
    @ApiOperation("点赞文章")
    public Boolean like(@RequestParam Integer id, HttpSession session){
        User user = userTool.getUser(session);
        List<Integer> like = user.getLikedList();
        if (like.contains(id)){
            return false;
        }

        like.add(0, id);
        user.setLikedList(like);
        dataTool.userRepo.save(user);

        Article article = dataTool.atcRepo.findById(id).get();
        article.setLikes(article.getLikes()+1);
        dataTool.atcRepo.save(article);

        return true;
    }
    @GetMapping("/light")
    @ApiOperation("点亮文章")
    public Boolean light(@RequestParam Integer id, HttpSession session){
        User user = userTool.getUser(session);
        List<Integer> light = user.getLightedList();
        if (light.contains(id) || user.getCandles() < 1){
            return false;
        }

        user.setCandles(user.getCandles()-1);
        light.add(0, id);
        user.setLightedList(light);
        dataTool.userRepo.save(user);

        Article article = dataTool.atcRepo.findById(id).get();
        article.setLights(article.getLights()+1);
        dataTool.atcRepo.save(article);

        return true;
    }
    @ApiOperation("收藏")
    @GetMapping("/favor")
    public Boolean favor(@RequestParam Integer id, HttpSession session){
        User user = userTool.getUser(session);
        List<Integer> favor = user.getFavorList();
        if (favor.contains(id)){
            return false;
        }
        favor.add(0, id);
        user.setFavorList(favor);
        dataTool.userRepo.save(user);

        Article article = dataTool.atcRepo.findById(id).get();
        article.setFavors(article.getFavors()+1);
        dataTool.atcRepo.save(article);

        return true;
    }
    @GetMapping("/unFavor")
    public Boolean unFavor(@RequestParam Integer id, HttpSession session){
        User user = userTool.getUser(session);
        List<Integer> favor = user.getFavorList();
        if (!favor.contains(id)){
            return false;
        }
        favor.remove(id);
        user.setFavorList(favor);
        dataTool.userRepo.save(user);

        Article article = dataTool.atcRepo.findById(id).get();
        article.setFavors(article.getFavors()-1);
        dataTool.atcRepo.save(article);

        return true;
    }

    //Add a new article
    @PostMapping("/post")
    @ApiOperation("投稿文章")
    public void addAtc(@RequestBody NotPassAtc article, HttpSession session){
        article.setTime();
        User user = userTool.getUser(session);
        article.setUid(user.getId());
        dataTool.notPassAtcRepo.save(article);
    }
    @PostMapping("/update")
    public Boolean update(@RequestBody Article article, HttpSession session){
        if (userTool.getUser(session).getId().equals(article.getUid())){
            dataTool.atcRepo.save(article);
            return true;
        }
        return false;
    }
    @PostMapping("/updateNP")
    public Boolean updateNP(@RequestBody NotPassAtc article, HttpSession session){
        if (userTool.getUser(session).getId().equals(article.getUid())){
            dataTool.notPassAtcRepo.save(article);
            return true;
        }
        return false;
    }
    @GetMapping("/del/{id}")
    public void delete(@PathVariable Integer id, HttpSession session){
        Article article = dataTool.atcRepo.findById(id).get();
        if (userTool.getUser(session).getId().equals(article.getUid())){
            dataTool.atcRepo.delete(article);
        }
    }
    @GetMapping("/delNP/{id}")
    public void delNP(@PathVariable Integer id, HttpSession session){
        NotPassAtc atc = dataTool.notPassAtcRepo.findById(id).get();
        if (userTool.getUser(session).getId().equals(atc.getUid())){
            dataTool.notPassAtcRepo.delete(atc);
        }
    }

}
