package cn.daman.chineseol.handler;

import cn.daman.chineseol.entity.Reply;
import cn.daman.chineseol.entity.User;
import cn.daman.chineseol.utils.DataTool;
import cn.daman.chineseol.utils.UserTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.*;

@RestController
@RequestMapping("/repl")
public class ReplHandler {
    @Autowired
    private DataTool dataTool;
    @Autowired
    private UserTool userTool;
    @GetMapping("/get/{aid}")
    public List<Map<String, Object>> getReply(@PathVariable Integer aid){
        //0 -> Msg
        List<Reply> list = dataTool.replyRepo.findByAid(aid);
        List<Map<String, Object>> mapList = new ArrayList<>();
        for (Reply r : list){
            Map<String, Object> map = new HashMap<>();
            map.put("content", r);
            map.put("uu", dataTool.userRepo.findById(r.getUid()).get());
            map.put("tu", dataTool.userRepo.findById(r.getTid()).get());
            mapList.add(map);
        }
        return mapList;
    }
    @PostMapping("/send")
    public void sendRepl(@RequestBody Reply reply, HttpSession session) {
        User user = userTool.getUser(session);
        reply.setUid(user.getId());
        reply.setTheTime(new Date());
        dataTool.replyRepo.save(reply);
    }
}
