package cn.daman.chineseol.handler;

import cn.daman.chineseol.entity.Article;
import cn.daman.chineseol.entity.User;

import cn.daman.chineseol.utils.DataTool;
import cn.daman.chineseol.utils.UserTool;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/user")
@Api("UserHandler")
public class UserHandler {
    @Autowired
    private DataTool dataTool;
    @Autowired
    private UserTool userTool;

    @PostMapping("/sign")
    @ApiOperation("注册")
    public Boolean sign(@RequestBody User user){
        //判断用户名和邮箱是否重复
        if (dataTool.userRepo.findByName(user.getName())!=null || dataTool.userRepo.findByEmail(user.getEmail())!=null){
            return false;
        }
        //初始化
        user.init();
        dataTool.userRepo.save(user);
        return true;
    }
    @ApiOperation("登录")
    @PostMapping("/login")
    public Boolean login(@RequestBody User user, HttpSession session, HttpServletResponse response){
        User u = dataTool.userRepo.findByEmail(user.getEmail());
        if (u!=null){
            if (u.getPassword().equals(user.getPassword())){
                session.setAttribute("uid", u.getId());
                session.setMaxInactiveInterval(-1);
                Cookie cookie = new Cookie("JSESSIONID",session.getId());
                cookie.setMaxAge(3600*24*14);
                cookie.setPath("/");
                response.addCookie(cookie);
                return true;
            }
        }
        return false;
    }
    @ApiOperation("登出")
    @GetMapping("/logout")
    public void logout(HttpServletRequest request, HttpServletResponse response, HttpSession session){
        Cookie[] cookies = request.getCookies();
        for (Cookie c : cookies){
            if (c.getName().equals("JSESSIONID")){
                c.setMaxAge(0);
                response.addCookie(c);
            }
        }
        session.removeAttribute("uid");
        session.setMaxInactiveInterval(0);
        session.invalidate();
    }
    @GetMapping("/getCandles")
    public Integer getCandles(HttpSession session){
        User user = userTool.getUser(session);
        return user.getCandles();
    }
    @GetMapping("/getFavors")
    public List<Article> getFavors(HttpSession session){
        User user = userTool.getUser(session);
        List<Integer> ints = user.getFavorList();
        List<Article> list = new ArrayList<>();
        for (Integer i : ints){
            list.add(dataTool.atcRepo.findById(i).get());
        }
        return list;
    }
    @GetMapping("/getUser")
    public User getUser(HttpSession session){
        return userTool.getUser(session);
    }
    @GetMapping("/getAvat/{id}")
    public String getAvat(@PathVariable Integer id) {
        return dataTool.userRepo.findById(id).get().getImg();
    }
    @GetMapping("/getUserBy/{id}")
    public User getUserBy(@PathVariable Integer id){
        return dataTool.userRepo.findById(id).get();
    }
    @GetMapping("/follow/{id}")
    public Boolean follow(@PathVariable Integer id, HttpSession session){
        User user = userTool.getUser(session);
        User u2 = dataTool.userRepo.findById(id).get();
        List<Integer> list = user.getFollow();
        List<Integer> l2 = u2.getFans();
        if (!list.contains(id) && !l2.contains(user.getId())){
            list.add(0, id);
            user.setFollow(list);
            dataTool.userRepo.save(user);

            l2.add(0, user.getId());
            u2.setFans(l2);
            dataTool.userRepo.save(u2);

            return true;
        }
        return false;
    }
    @GetMapping("/unfollow/{id}")
    public Boolean unfollow(@PathVariable Integer id, HttpSession session){
        User user = userTool.getUser(session);
        User u2 = dataTool.userRepo.findById(id).get();
        List<Integer> list = user.getFollow();
        List<Integer> l2 = u2.getFans();
        if (list.contains(id) && l2.contains(user.getId())){
            list.remove(id);
            user.setFollow(list);
            dataTool.userRepo.save(user);

            l2.remove(user.getId());
            u2.setFans(l2);
            dataTool.userRepo.save(u2);

            return true;
        }
        return false;
    }
    @GetMapping("/followList")
    public Map<String, List<User>> getFollowList(HttpSession session){
        User user = userTool.getUser(session);
        List<Integer> follow = user.getFollow();
        List<Integer> fans = user.getFans();
        Map<String, List<User>> map = new HashMap<>();
        List<User> list1 = new ArrayList<>();
        List<User> list2 = new ArrayList<>();
        for (Integer i : follow){
            list1.add(dataTool.userRepo.findById(i).get());
        }
        for (Integer i : fans){
            list2.add(dataTool.userRepo.findById(i).get());
        }
        map.put("follow", list1);
        map.put("fans", list2);
        return map;
    }
}
