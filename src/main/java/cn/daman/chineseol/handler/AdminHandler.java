package cn.daman.chineseol.handler;

import cn.daman.chineseol.entity.Article;
import cn.daman.chineseol.entity.NotPassAtc;
import cn.daman.chineseol.entity.User;
import cn.daman.chineseol.utils.DataTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/admin")
public class AdminHandler {
    @Autowired
    private DataTool dataTool;
    @GetMapping("/pass/{id}")
    public void passAtc(@PathVariable Integer id){
        Article article = new Article();
        article.parse(dataTool.notPassAtcRepo.findById(id).get());
        article.init();
        dataTool.atcRepo.save(article);
        dataTool.notPassAtcRepo.deleteById(id);
    }
    @GetMapping("/passAll")
    public void passAll(){
        List<NotPassAtc> list = dataTool.notPassAtcRepo.findAll();
        for (NotPassAtc a : list){
            Article article = new Article();
            article.parse(a);
            article.init();
            dataTool.atcRepo.save(article);
            dataTool.notPassAtcRepo.deleteById(a.getId());
        }
    }
    @GetMapping("/setTop/{id}")
    public void setTop(@PathVariable Integer id){
        Article article = dataTool.atcRepo.findById(id).get();
        article.setTop(true);
        dataTool.atcRepo.save(article);
    }
    @GetMapping("/getAllNP")
    public List<Map<String, Object>> getAllNP(){
        List<Map<String, Object>> arr = new ArrayList<>();
        List<NotPassAtc> list = dataTool.notPassAtcRepo.findAllByOrderByTheTimeDesc();
        for (NotPassAtc a : list){
            Map<String, Object> map = new HashMap<>();
            map.put("content", a);
            map.put("user", dataTool.userRepo.findById(a.getUid()).get());
            arr.add(map);
        }
        return arr;
    }
    @GetMapping("/del/{id}")
    public void delete(@PathVariable Integer id){
        dataTool.atcRepo.deleteById(id);
    }
    @GetMapping("/delNP/{id}")
    public void delNP(@PathVariable Integer id){
        dataTool.notPassAtcRepo.deleteById(id);
    }
    @GetMapping("/getAllUsers")
    public List<User> getAllUsers(){
        return dataTool.userRepo.findAllByOrderByTheTimeDesc();
    }
    @GetMapping("/getAllAdmins")
    public List<User> getAllAdmins(){
        List<User> list = new ArrayList<>();
        list.addAll(dataTool.userRepo.findByTypeOrderByTheTimeDesc(-1));
        list.addAll(dataTool.userRepo.findByTypeOrderByTheTimeDesc(0));
        return list;
    }
    @GetMapping("/add")
    public Boolean addAdmin(@RequestParam String em, @RequestParam Integer type){
        User user = dataTool.userRepo.findByEmail(em);
        if (user==null || !user.getType().equals(1)){
            return false;
        }
        user.setType(type);
        dataTool.userRepo.save(user);
        return true;
    }
    @GetMapping("/revoke")
    public Boolean revoke(@RequestParam Integer id){
        User user = dataTool.userRepo.findById(id).get();
        user.setType(1);
        dataTool.userRepo.save(user);
        return true;
    }
}
