package cn.daman.chineseol.handler;

import cn.daman.chineseol.entity.Article;
import cn.daman.chineseol.entity.NotPassAtc;
import cn.daman.chineseol.entity.User;
import cn.daman.chineseol.utils.DataTool;
import cn.daman.chineseol.utils.UserTool;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.*;

@RestController
@Api("ArticleHandler")
@RequestMapping("/atc")
public class AtcHandler {
    @Autowired
    private DataTool dataTool;
    @Autowired
    private UserTool userTool;
    //Index page
    @ApiOperation("首页文章")
    @GetMapping("/getIndex")
    public Page<Article> getIndex(@RequestParam int page, @RequestParam int size){
        Pageable pageable = PageRequest.of(page-1,size, Sort.Direction.DESC,"theTime");
        Page<Article> list = dataTool.atcRepo.findAll(pageable);
        return list;
    }
    //Get by id
    @GetMapping("/getById/{id}")
    @ApiOperation("通过id获取文章")
    public Article getById(@PathVariable Integer id){
        Article article = dataTool.atcRepo.findById(id).get();
        Integer v = article.getViews();
        article.setViews(v+1);
        dataTool.atcRepo.save(article);
        return article;
    }
    @GetMapping("/preview/{id}")
    public Article preview(@PathVariable Integer id){
        return dataTool.atcRepo.findById(id).get();
    }

    @GetMapping("/getTop")
    @ApiOperation("获得置顶文章")
    public List<Article> getTop(){
        return dataTool.atcRepo.findByTop(true);
    }
    @GetMapping("/getByLights")
    public Page<Article> getByLights(@RequestParam(defaultValue = "5") Integer size){
        Pageable pageable = PageRequest.of(0,size, Sort.Direction.DESC,"lights");
        Page<Article> list = dataTool.atcRepo.findAll(pageable);
        return list;
    }
    @GetMapping("/getByLikes")
    public Page<Article> getByLikes(@RequestParam(defaultValue = "5") Integer size){
        Pageable pageable = PageRequest.of(0,size, Sort.Direction.DESC, "likes");
        Page<Article> list = dataTool.atcRepo.findAll(pageable);
        return list;
    }
    @GetMapping("/search")
    public List<Article> search(@RequestParam String wd) {
        List<Article> list = dataTool.atcRepo.findAllByOrderByTheTimeDesc();
        List<Article> arr = new ArrayList<>();
        wd = wd.toLowerCase();
        for (Article a : list) {
            if (a.getTitle().toLowerCase().contains(wd) || a.getDes().toLowerCase().contains(wd)){
                arr.add(a);
            }
        }
        return arr;
    }
    @GetMapping("/getByUid/{id}")
    public List<Article> getByUid(@PathVariable Integer id) {
        return dataTool.atcRepo.findByUidOrderByTheTimeDesc(id);
    }
    @GetMapping("/getInfoByUid/{uid}")
    public Map<String, Integer> getInfoByUid(@PathVariable Integer uid){
        Map<String, Integer> map = new HashMap<>();
        List<Article> list = dataTool.atcRepo.findByUidOrderByTheTimeDesc(uid);
        Integer views = 0;
        Integer likes = 0;
        Integer lights = 0;
        for (Article a : list){
            views += a.getViews();
            likes += a.getLikes();
            lights += a.getLights();
        }
        map.put("views", views);
        map.put("likes", likes);
        map.put("lights", lights);
        return map;
    }
    @GetMapping("/getMyInfo")
    public Map<String, Integer> getMyViews(HttpSession session){
        return getInfoByUid(userTool.getUser(session).getId());
    }
    @GetMapping("/getMyAtc")
    public List<Article> getMyAtc(HttpSession session){
        return dataTool.atcRepo.findByUidOrderByTheTimeDesc(userTool.getUser(session).getId());
    }
    @GetMapping("/getMyNotPass")
    public List<NotPassAtc> getMyNotPass(HttpSession session){
        return dataTool.notPassAtcRepo.findByUidOrderByTheTimeDesc(userTool.getUser(session).getId());
    }
    @GetMapping("/np/getById/{id}")
    public NotPassAtc getNotPassById(@PathVariable Integer id){
        return dataTool.notPassAtcRepo.findById(id).get();
    }
    @GetMapping("/getMyFavor")
    public List<Article> getMyFavor(HttpSession session){
        List<Integer> favor = userTool.getUser(session).getFavorList();
        List<Article> list = new ArrayList<>();
        for (Integer i : favor){
            list.add(dataTool.atcRepo.findById(i).get());
        }
        return list;
    }
    @GetMapping("/getAll")
    public List<Map<String, Object>> getAll(){
        List<Map<String, Object>> arr = new ArrayList<>();
        List<Article> list = dataTool.atcRepo.findAllByOrderByTheTimeDesc();
        for (Article a : list){
            Map<String, Object> map = new HashMap<>();
            map.put("content", a);
            map.put("user", dataTool.userRepo.findById(a.getUid()).get());
            arr.add(map);
        }
        return arr;
    }
    @GetMapping("/getTot")
    public Map<String, Integer> getTotal(){
        List<Article> list = dataTool.atcRepo.findAllByOrderByTheTimeDesc();
        Integer views = 0;
        Integer likes = 0;
        for (Article a : list){
            views += a.getViews();
            likes += a.getLikes();
        }
        Map<String, Integer> map = new HashMap<>();
        map.put("views", views);
        map.put("likes", likes);
        return map;
    }
}
