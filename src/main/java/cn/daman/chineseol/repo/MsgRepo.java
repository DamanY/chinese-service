package cn.daman.chineseol.repo;

import cn.daman.chineseol.entity.Message;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MsgRepo extends JpaRepository<Message, Integer> {
}
