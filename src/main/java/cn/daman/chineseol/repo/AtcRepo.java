package cn.daman.chineseol.repo;

import cn.daman.chineseol.entity.Article;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AtcRepo extends JpaRepository<Article, Integer> {
    List<Article> findByTop(Boolean top);

    List<Article> findAllByOrderByTheTimeDesc();

    List<Article> findByUidOrderByTheTimeDesc(Integer uid);
}
