package cn.daman.chineseol.repo;

import cn.daman.chineseol.entity.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CmtRepo extends JpaRepository<Comment, Integer> {
    @Query("select c from Comment c where c.belong = ?1 order by c.theTime desc")
    List<Comment> findByBelong(Integer belong);

    List<Comment> findByUidOrderByTheTimeDesc(Integer uid);
}
