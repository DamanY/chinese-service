package cn.daman.chineseol.repo;

import cn.daman.chineseol.entity.Reply;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ReplyRepo extends JpaRepository<Reply, Integer> {
    @Query("select r from Reply r where r.aid = ?1 order by r.theTime desc")
    List<Reply> findByAid(Integer aid);
}
