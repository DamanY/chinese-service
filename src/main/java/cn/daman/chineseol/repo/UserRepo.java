package cn.daman.chineseol.repo;

import cn.daman.chineseol.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepo extends JpaRepository<User, Integer> {
    User findByName(String name);
    User findByEmail(String email);

    List<User> findAllByOrderByTheTimeDesc();

    List<User> findByTypeOrderByTheTimeDesc(Integer type);
}
