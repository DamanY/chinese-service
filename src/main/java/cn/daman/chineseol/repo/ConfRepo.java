package cn.daman.chineseol.repo;

import cn.daman.chineseol.entity.Config;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ConfRepo extends JpaRepository<Config, Integer> {
    Config findByTheKey(String key);
}
