package cn.daman.chineseol.repo;

import cn.daman.chineseol.entity.NotPassAtc;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface NotPassAtcRepo extends JpaRepository<NotPassAtc, Integer> {
    Optional<NotPassAtc> findById(Integer id);

    List<NotPassAtc> findByUidOrderByTheTimeDesc(Integer uid);

    List<NotPassAtc> findAllByOrderByTheTimeDesc();
}
