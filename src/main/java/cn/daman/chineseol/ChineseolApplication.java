package cn.daman.chineseol;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChineseolApplication {

    public static void main(String[] args) {
        SpringApplication.run(ChineseolApplication.class, args);
    }

}
