package cn.daman.chineseol.entity;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;

import javax.persistence.AttributeConverter;

public class JpaConverterListJson implements AttributeConverter<Object, String> {
    @SneakyThrows
    @Override
    public String convertToDatabaseColumn(Object o) {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(o);
    }

    @SneakyThrows
    @Override
    public Object convertToEntityAttribute(String s) {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(s, Object.class);
    }
}
