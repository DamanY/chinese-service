package cn.daman.chineseol.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Data
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String password;
    private Integer type;
    private String email;
    private Integer clazz;
    private Integer sess;
    //1-student, 2-guest
    //0-admin, -1-super admin,

    private Date theTime;
    private String img;
    @Convert(converter = JpaConverterListJson.class)
    private List<Integer> favorList;
    @Convert(converter = JpaConverterListJson.class)
    private List<Integer> lightedList;
    @Convert(converter = JpaConverterListJson.class)
    private List<Integer> likedList;

    @Convert(converter = JpaConverterListJson.class)
    private List<Integer> follow;
    @Convert(converter = JpaConverterListJson.class)
    private List<Integer> fans;

    private Integer candles;

    public void init(){
        this.theTime = new Date();
        this.favorList = new ArrayList<>();
        this.lightedList = new ArrayList<>();
        this.likedList = new ArrayList<>();
        this.candles = 0;

        this.follow = new ArrayList<>();
        this.fans = new ArrayList<>();
    }
}
