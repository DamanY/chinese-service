package cn.daman.chineseol.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
public class NotPassAtc {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String title;
//    private String author;
    private Integer uid;
    private String tag;
    private String text;
    private String des;
    private String img;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
    private Date theTime;

    public void setTime(){
        this.theTime = new Date();
    }
}
