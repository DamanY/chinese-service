package cn.daman.chineseol.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity
@Data
public class Article{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String title;
//    private String author;
    private Integer uid;
    private String tag;
    private String text;
    private String des;
    private String img;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
    private Date theTime;

    private Integer views;
    private Integer favors;
    private Integer lights;
    private Integer likes;

    private Boolean top;

    public void init(){
        this.setId(null);
        views = 0;
        favors = 0;
        lights = 0;
        likes = 0;

        top = false;
    }

    public void parse(NotPassAtc atc){
//        id = null;
        title = atc.getTitle();
//        author = atc.getAuthor();
        uid = atc.getUid();
        tag = atc.getTag();
        text = atc.getText();
        des = atc.getDes();
        img = atc.getImg();
        theTime = atc.getTheTime();
    }

}
